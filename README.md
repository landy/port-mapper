# What's this?

This is a quick and dirty utility I made to allow me to quickly set port descriptions and map switch ports to patch panels. 
It uses flask to create a basic web interface so you can use this in the browser.

# Getting Started

Install the requirements with pip

```bash
pip install -r requirements.txt
```

Create your settings file

```bash
cp settings.json.example settings.json
```

Modify `settings.json` with your netbox server URL and access token.

```bash
vi settings.json
```

Run the flask server

```bash
export FLASK_APP=app.py
flask run --port 5000
```

Open your web browser and go to http://localhost:5000