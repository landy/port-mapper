from flask import Flask
from flask import request
from flask import render_template

import requests
import json

app = Flask(__name__)

# Load config data
try:
    with open('settings.json', 'r') as f:
        settings = json.load(f)
except Exception:
    print("Error loading settings. Verify that settings.json exists and contains valid json!")
    exit(-1)


headers_ro = {
    'Authorization': 'Token ' + settings['netbox']['token'],
    'accept': 'application/json'
}
headers_rw = {
    'Authorization': 'Token ' + settings['netbox']['token'],
    'accept': 'application/json',
    'Content-Type': 'application/json'
}


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/rename/', methods=['POST', 'GET'])
@app.route('/rename/<deviceid>/', methods=['POST', 'GET'])
def rename(deviceid=None):
    device = None
    devices = None
    ports = None
    goodports = []

    if deviceid is None and request.method == 'POST':
        payload = {'q': request.form['device']}
        r = requests.get(settings['netbox']['api'] + '/dcim/devices', headers=headers_ro, params=payload)
        devices = r.json()

        if devices['count'] == 0:
            devices = None

    if deviceid is not None:
        # Device
        r = requests.get(settings['netbox']['api'] + '/dcim/devices/' + str(deviceid), headers=headers_ro)
        device = r.json()

        if request.method == 'POST':
            for field in request.form:
                type = field.split(':')[0]
                id = field.split(':')[1]

                payload = {'description': request.form[field]}

                if 'frontport' in type:
                    r = requests.patch(settings['netbox']['api'] + '/dcim/front-ports/' + id + '/', headers=headers_rw, json=payload)
                    if r.status_code == 200:
                        goodports.append(r.json())
                elif 'interface' in type:
                    r = requests.patch(settings['netbox']['api'] + '/dcim/interfaces/' + id + '/', headers=headers_rw, json=payload)
                    if r.status_code == 200:
                        goodports.append(r.json())

        # From Front Ports
        payload = {'device_id': deviceid, 'limit': 1000}
        r = requests.get(settings['netbox']['api'] + '/dcim/front-ports', headers=headers_ro, params=payload)
        from_front_ports = r.json()

        # From Interfaces
        payload = {'device_id': deviceid, 'limit': 1000}
        r = requests.get(settings['netbox']['api'] + '/dcim/interfaces', headers=headers_ro, params=payload)
        from_interfaces = r.json()

        # Combine Lists
        ports = []

        if 'results' in from_front_ports:
            for port in from_front_ports['results']:
                port['termtype'] = 'frontport'
                ports.append(port)

        if 'results' in from_interfaces:
            for port in from_interfaces['results']:
                port['termtype'] = 'interface'
                ports.append(port)

    return render_template('rename.html', device=device, devices=devices, ports=ports, goodports=goodports)


@app.route('/connect/', methods=['POST', 'GET'])
def preconnect():
    data = None
    if request.method == 'POST':
        payload = {'q': request.form['from']}
        r = requests.get(settings['netbox']['api'] + '/dcim/devices', headers=headers_ro, params=payload)
        data = r.json()

        if data['count'] == 0:
            data = None

    return render_template('preconnect.html', devices=data)


@app.route('/connect/<int:fromdev>/', methods=['POST', 'GET'])
def connect(fromdev=None):
    data = None
    if request.method == 'POST':
        payload = {'q': request.form['to']}
        r = requests.get(settings['netbox']['api'] + '/dcim/devices', headers=headers_ro, params=payload)
        data = r.json()

        if data['count'] == 0:
            data = None

    return render_template('connect.html', fromdev=fromdev, devices=data)


@app.route('/ajax/map/ports/', methods=['POST'])
def map_ports():
    created = []
    if request.method == 'POST':
        cables = request.json
        for cable in cables:
            payload = {
                'termination_a_type': cable['a_type'],
                'termination_a_id': int(cable['a_id']),
                'termination_b_type': cable['b_type'],
                'termination_b_id': int(cable['b_id']),
                'type': int(cable['type']),
                'color': cable['color']
            }
            if len(cable['cable_len']) > 0:
                payload['length'] = int(cable['cable_len'])
                payload['length_unit'] = int(cable['len_unit'])

            print(payload)

            r = requests.post(settings['netbox']['api'] + '/dcim/cables/', headers=headers_rw, json=payload)
            if r.status_code == 201:
                data = r.json()
                created.append(data)
            else:
                print(r.text)

    return json.dumps(created)


@app.route('/connect/<int:fromdev>/to/<int:todev>/', methods=['POST', 'GET'])
def connectto(fromdev=None, todev=None):
    # From Front Ports
    payload = {'device_id': fromdev, 'limit': 1000}
    r = requests.get(settings['netbox']['api'] + '/dcim/front-ports', headers=headers_ro, params=payload)
    from_front_ports = r.json()

    # From Interfaces
    payload = {'device_id': fromdev, 'limit': 1000}
    r = requests.get(settings['netbox']['api'] + '/dcim/interfaces', headers=headers_ro, params=payload)
    from_interfaces = r.json()

    # To Front Ports
    payload = {'device_id': todev, 'limit': 1000}
    r = requests.get(settings['netbox']['api'] + '/dcim/front-ports', headers=headers_ro, params=payload)
    to_front_ports = r.json()

    # To Interfaces
    payload = {'device_id': todev, 'limit': 1000}
    r = requests.get(settings['netbox']['api'] + '/dcim/interfaces', headers=headers_ro, params=payload)
    to_interfaces = r.json()

    # Combine Lists
    from_slots = []
    to_slots = []

    if 'results' in from_front_ports:
        for port in from_front_ports['results']:
            port['termtype'] = 'dcim.frontport'
            from_slots.append(port)

    if 'results' in from_interfaces:
        for port in from_interfaces['results']:
            port['termtype'] = 'dcim.interface'
            from_slots.append(port)

    if 'results' in to_front_ports:
        for port in to_front_ports['results']:
            port['termtype'] = 'dcim.frontport'
            to_slots.append(port)

    if 'results' in to_interfaces:
        for port in to_interfaces['results']:
            port['termtype'] = 'dcim.interface'
            to_slots.append(port)

    # Choices
    r = requests.get(settings['netbox']['api'] + '/dcim/_choices/cable%3Alength_unit', headers=headers_ro)
    length_units = r.json()

    r = requests.get(settings['netbox']['api'] + '/dcim/_choices/cable%3Atype', headers=headers_ro)
    cable_types = r.json()

    # Devices
    r = requests.get(settings['netbox']['api'] + '/dcim/devices/' + str(fromdev), headers=headers_ro)
    from_dev = r.json()

    r = requests.get(settings['netbox']['api'] + '/dcim/devices/' + str(todev), headers=headers_ro)
    to_dev = r.json()

    return render_template('connectto.html', from_slots=from_slots, to_slots=to_slots, from_dev=from_dev,
                           to_dev=to_dev, length_units=length_units, cable_types=cable_types, colors=COLOR_CHOICES)


COLOR_CHOICES = (
    ('aa1409', 'Dark red'),
    ('f44336', 'Red'),
    ('e91e63', 'Pink'),
    ('ffe4e1', 'Rose'),
    ('ff66ff', 'Fuschia'),
    ('9c27b0', 'Purple'),
    ('673ab7', 'Dark purple'),
    ('3f51b5', 'Indigo'),
    ('2196f3', 'Blue'),
    ('03a9f4', 'Light blue'),
    ('00bcd4', 'Cyan'),
    ('009688', 'Teal'),
    ('00ffff', 'Aqua'),
    ('2f6a31', 'Dark green'),
    ('4caf50', 'Green'),
    ('8bc34a', 'Light green'),
    ('cddc39', 'Lime'),
    ('ffeb3b', 'Yellow'),
    ('ffc107', 'Amber'),
    ('ff9800', 'Orange'),
    ('ff5722', 'Dark orange'),
    ('795548', 'Brown'),
    ('c0c0c0', 'Light grey'),
    ('9e9e9e', 'Grey'),
    ('607d8b', 'Dark grey'),
    ('111111', 'Black'),
    ('ffffff', 'White'),
)